import asss

game = asss.get_interface(asss.I_GAME)


def flagReset(arena, freq, points):
        game.ShipReset(arena)
        game.GivePrize(arena, asss.PRIZE_WARP, 1)


def mm_attach(arena):
        arena.flagreset_shipresetwarp_callback = asss.reg_callback(asss.CB_FLAGRESET, flagReset, arena)


def mm_detach(arena):
        delattr(arena, 'flagreset_shipresetwarp_callback')


