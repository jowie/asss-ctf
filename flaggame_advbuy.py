import asss

lm = asss.get_interface(asss.I_LOGMAN)

class FlagGameAdvBuy:
        def __init__(self, arena):
                self.arena = arena
                self.flagresetCallback = None
                self.running = False

        def attach(self):
                self.flagresetCallback = asss.reg_callback(asss.CB_FLAGRESET, self.flagReset, self.arena)
                self.start()

        def detach(self):
                self.stop()
                self.flagresetCallback = None

        def flagReset(self, arena, freq, points):
                self.stop()
                self.start()
                pass

        def start(self):
                if self.running:
                        return
                self.running = True

                lm.LogA(asss.L_INFO, 'flaggame_advbuy', self.arena, 'Starting game')

                result = False
                gamecredits = asss.get_interface(asss.I_GAMECREDITS, self.arena)
                if gamecredits:
                        result = gamecredits.StartGame(self.arena)

                if not result:
                        lm.LogA(asss.L_INFO, 'flaggame_advbuy', self.arena, 'Starting I_GAMECREDITS failed')
                
                if hasattr(asss, 'I_STATICTURRET'):
                        staticturret = asss.get_interface(asss.I_STATICTURRET, self.arena)
                        if staticturret:
                                staticturret.StartGame(self.arena)

                result = False
                advbuy = asss.get_interface(asss.I_ADVBUY, self.arena)
                if advbuy:
                        result = advbuy.StartGame(self.arena)

                if not result:
                        lm.LogA(asss.L_INFO, 'flaggame_advbuy', self.arena, 'Starting I_ADVBUY failed')

        def stop(self):
                if not self.running:
                        return
                self.running = False

                lm.LogA(asss.L_INFO, 'flaggame_advbuy', self.arena, 'Stopping game')

                advbuy = asss.get_interface(asss.I_ADVBUY, self.arena)
                if advbuy:
                        advbuy.StopGame(self.arena)
                
                if hasattr(asss, 'I_STATICTURRET'):
                        staticturret = asss.get_interface(asss.I_STATICTURRET, self.arena)
                        if staticturret:
                                staticturret.StopGame(self.arena)
                
                gamecredits = asss.get_interface(asss.I_GAMECREDITS, self.arena)
                if gamecredits:
                        gamecredits.StopGame(self.arena)


def mm_attach(arena):
        arena.flaggame_advbuy = FlagGameAdvBuy(arena)
        arena.flaggame_advbuy.attach()


def mm_detach(arena):
        if arena.flaggame_advbuy:
                arena.flaggame_advbuy.detach()

        delattr(arena, 'flaggame_advbuy')
