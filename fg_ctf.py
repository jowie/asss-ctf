# JoWie <jowie@welcome-to-the-machine>
# This file is part of ASSS CTF
#
# ASSS CTF is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ASSS CTF is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ASSS CTF.  If not, see <http://www.gnu.org/licenses/>.
#
# In addition, the following supplemental terms based on section 7 of
# the GNU General Public License (version 3):
# a) Preservation of all legal notices and author attributions
# b) Prohibition of misrepresentation of the origin of this material, and
# modified versions are required to be marked in reasonable ways as
# different from the original version

import asss
# from pprint import pprint

flagcore = asss.get_interface(asss.I_FLAGCORE)
cfg = asss.get_interface(asss.I_CONFIG)
# lm = asss.get_interface(asss.I_LOGMAN)
chat = asss.get_interface(asss.I_CHAT)
mapdata = asss.get_interface(asss.I_MAPDATA)


class TeamConfig:
        def __init__(self):
                self.x = 0
                self.y = 0
                self.region = None
                self.name = ''

        def load(self, arena, freq):
                c = arena.cfg
                self.x = cfg.GetInt(c, 'CTF', 'Team%d-X' % freq, 502)
                self.y = cfg.GetInt(c, 'CTF', 'Team%d-Y' % freq, 512)
                regionName = cfg.GetStr(c, 'CTF', 'Team%d-Region' % freq)
                self.region = mapdata.FindRegionByName(arena, regionName) if regionName else None
                self.name = cfg.GetStr(c, 'CTF', 'Team%d-Name' % freq)

                if not self.name:
                        self.name = 'Team %d' % freq


class Config:

        # cfghelp: CTF:Team0-X, arena, int, def: 502
        # The X coordinate where the flag for this team will spawn (in tiles)

        # cfghelp: CTF:Team0-Y, arena, int, def: 512
        # The Y coordinate where the flag for this team will spawn (in tiles)

        # cfghelp: CTF:Team0-Region, arena, int
        # The region where an enemy player can score

        # cfghelp: CTF:Team0-Name, arena, string, def: Team 0
        # The name for this freq to display in arena messages

        # cfghelp: CTF:Team1-X, arena, int, def: 502
        # The X coordinate where the flag for this team will spawn (in tiles)

        # cfghelp: CTF:Team1-Y, arena, int, def: 512
        # The Y coordinate where the flag for this team will spawn (in tiles)

        # cfghelp: CTF:Team1-Region, arena, int
        # The region where an enemy player can score

        # cfghelp: CTF:Team1-Name, arena, string, def: Team 0
        # The name for this freq to display in arena messages

        # cfghelp: CTF:WinCaptures, arena, int, def: 3
        # How many flag captures a team needs to win the game

        # cfghelp: CTF:NeutAfterKill, arena, bool, def: 0
        # If enabled, a killed flagger drops his flag on the ground instead of resetting it
        # An enemy can then steal it, or a friendly can return it home by touching it

        def __init__(self):
                self.teams = 2
                self.team = []

                for freq in xrange(self.teams):
                        self.team.append(TeamConfig())

                self.winCaptures = 1
                self.neutAfterKill = False

        def load(self, arena):
                c = arena.cfg

                for freq, config in enumerate(self.team):
                        config.load(arena, freq)

                self.winCaptures = cfg.GetInt(c, 'CTF', 'WinCaptures', 3)

                if self.winCaptures < 1:
                        self.winCaptures = 1

                self.neutAfterKill = not not cfg.GetInt(c, 'CTF', 'NeutAfterKill', 0)

class FlagGameInterface:
        iid = asss.I_FLAGGAME

        def __init__(self, ctf):
                self.ctf = ctf

        def Init(self, arena):
                self.ctf.flagInit()

        def FlagTouch(self, arena, player, flagId):
                self.ctf.flagTouch(player, flagId)

        def Cleanup(self, arena, flagId, reason, oldCarrier, oldFreq):
                self.ctf.flagCleanup(flagId, reason, oldCarrier, oldFreq)


class CTF:
        def __init__(self, arena):
                self.arena = arena
                self.config = Config()
                self.flagGameInterface = None
                self.arenaActionCallback = None
                self.warpCallback = None
                self.attachCallback = None
                self.tickTimer = None
                self.teamScores = []

                assert self.config.teams == len(self.config.team)

                for _ in xrange(self.config.teams):
                        self.teamScores.append(0)

        def attach(self):
                self.config.load(self.arena)

                # When these references are gone, pymod unregisters the interfaces/callbacks
                self.flagGameInterface = asss.reg_interface(FlagGameInterface(self), self.arena)
                self.arenaActionCallback = asss.reg_callback(asss.CB_ARENAACTION, self.arenaAction, self.arena)
                self.warpCallback = asss.reg_callback(asss.CB_WARP, self.playerWarp, self.arena)  # asss jowie fork 1.5.4
                self.attachCallback = asss.reg_callback(asss.CB_ATTACH, self.playerAttach, self.arena)
                self.tickTimer = asss.set_timer(self.tick, 1)

        def detach(self):
                self.flagGameInterface = None
                self.arenaActionCallback = None
                self.tickTimer = None
                self.warpCallback = None
                self.attachCallback = None
                self.tickTimer = None
                self.arena = None

        def arenaAction(self, arena, action):
                assert arena == self.arena

                if action == asss.AA_CONFCHANGED:
                        self.config.load(arena)

        def playerWarp(self, player, oldX, oldY, newX, newY):
                if player.flagscarried > 0 and \
                   0 <= player.freq < self.config.teams:
                        self.resetCarriedFlags(player)

        def playerAttach(self, player, playerTo):
                if player.flagscarried > 0 and \
                   0 <= player.freq < self.config.teams:
                        self.resetCarriedFlags(player)

        def tick(self):
                asss.for_each_player(self.tickPlayer)

                return True  # keep running

        def tickPlayer(self, player):
                if player.arena != self.arena:
                        return

                freq = player.freq

                if player.flagscarried <= 0:
                        return

                if freq < 0 or freq >= self.config.teams:
                        return
                
                x, y, _, _, _, _, status = player.position

                if status & asss.STATUS_FLASH:  # might be warping
                        return

                for enemyFreq in xrange(self.config.teams):
                        if enemyFreq != freq and \
                           self.isPositionAtHome(freq, x / 16, y / 16) and \
                           self.isTeamFlagAtHome(freq):

                                self.teamScore(player, freq, enemyFreq)

        def flagInit(self):
                flagcore.SetCarryMode(self.arena, 1)
                flagcore.ReserveFlags(self.arena, self.config.teams)
                self.resetAllFlags()

        def flagTouch(self, player, flagId):
                flagFreq = flagId
                if flagFreq == player.freq:
                        # friendly pickup, return it home
                        self.resetTeamFlag(flagFreq)
                        asss.call_callback(asss.CB_CTFSAVE, (self.arena, player, flagFreq), self.arena)
                else:
                        # assign him the flag
                        self.giveFlag(player, flagId)

                        chat.SendAnyMessage(
                                arenaToPlayerList(self.arena),
                                asss.MSG_SYSOPWARNING,
                                asss.SOUND_BEEP3,
                                None,
                                'The %s flag was stolen by %s!' %
                                (self.getTeamName(flagId), player.name)
                        )

        def flagCleanup(self, flagId, reason, oldCarrier, oldFreq):
                if flagId < self.config.teams:
                        freq = flagId

                        if oldCarrier is not None and \
                           self.config.neutAfterKill and \
                                (reason == asss.CLEANUP_KILL_NORMAL or
                                 reason == asss.CLEANUP_KILL_TK or
                                 reason == asss.CLEANUP_KILL_CANTCARRY or
                                 reason == asss.CLEANUP_KILL_FAKE):

                                # neut in place
                                x, y, _, _, _, _, _ = oldCarrier.position
                                self.neutFlag(flagId, x / 16, y / 16)

                        else:
                                # place at home
                                self.resetTeamFlag(freq)

        def isPositionAtHome(self, freq, x, y):
                if freq < 0 or freq >= self.config.teams:
                        return False

                # -1 is often used to for uninitialized positions in asss:
                if x < 0 or y < 0:
                        return False

                teamConfig = self.config.team[freq]

                return teamConfig.region and mapdata.Contains(teamConfig.region, x, y)

        def isTeamFlagAtHome(self, freq):
                flagId = freq
                if freq < 0 or freq >= self.config.teams:
                        return False

                count, flagInfo = flagcore.GetFlags(self.arena, flagId)

                assert count < 2

                if count < 1:
                        return False

                return self.isPositionAtHome(freq, flagInfo.x, flagInfo.y)

        def resetTeamFlag(self, freq):
                flagId = freq

                flagInfo = asss.flaginfo()
                flagInfo.state = asss.FI_ONMAP
                flagInfo.x = self.config.team[freq].x
                flagInfo.y = self.config.team[freq].y
                flagInfo.freq = freq
                flagInfo.carrier = None

                flagcore.SetFlags(self.arena, flagId, flagInfo)

        def giveFlag(self, player, flagId):
                flagInfo = asss.flaginfo()
                flagInfo.state = asss.FI_CARRIED
                flagInfo.carrier = player
                flagcore.SetFlags(self.arena, flagId, flagInfo)

        def neutFlag(self, flagId, tileX, tileY):
                flagInfo = asss.flaginfo()
                flagInfo.state = asss.FI_ONMAP
                flagInfo.x = tileX
                flagInfo.y = tileY
                flagInfo.freq = -1
                flagInfo.carrier = None

                flagcore.SetFlags(self.arena, flagId, flagInfo)

        def resetAllFlags(self):
                for freq in xrange(self.config.teams):
                        self.resetTeamFlag(freq)

        def resetCarriedFlags(self, player):
                for flagId in xrange(256):
                        hasFlag, flagInfo = flagcore.GetFlags(self.arena, flagId)
                        assert hasFlag < 2
                        if not hasFlag:
                                break

                        if flagInfo.carrier == player:
                                flagFreq = flagId % self.config.teams
                                flagInfo.state = asss.FI_ONMAP
                                flagInfo.x = self.config.team[flagFreq].x
                                flagInfo.y = self.config.team[flagFreq].y
                                flagInfo.freq = flagFreq
                                flagInfo.carrier = None
                                flagcore.SetFlags(self.arena, flagId, flagInfo)

        def teamScore(self, scoredBy, freq, enemyFreq):
                # "freq" scored at "enemyFreq" thanks to "scoredBy"
                self.resetTeamFlag(enemyFreq)

                self.teamScores[freq] += 1

                asss.call_callback(asss.CB_CTFSCORE, (self.arena, scoredBy, freq, enemyFreq, self.teamScores[freq]), self.arena)

                if self.teamScores[freq] >= self.config.winCaptures:
                        chat.SendArenaSoundMessage(
                                self.arena,
                                asss.SOUND_BEEP3,
                                '%s captured the flag for %s!' %
                                        (scoredBy.name, self.getTeamName(freq))
                        )

                        self.teamWin(freq)
                else:
                        capturesNeeded = self.config.winCaptures - self.teamScores[freq]
                        chat.SendArenaSoundMessage(
                                self.arena,
                                asss.SOUND_BEEP3,
                                '%s captured the flag for %s! Needs %d more capture%s to win.' %
                                        (scoredBy.name, self.getTeamName(freq), capturesNeeded, '' if capturesNeeded == 1 else 's')
                        )

        def teamWin(self, freq):
                chat.SendArenaSoundMessage(
                        self.arena,
                        asss.SOUND_DING,
                        'NOTICE: Game over. %s wins! Final score: %s' %
                                (self.getTeamName(freq), ' - '.join(str(x) for x in self.teamScores))
                )

                for i in xrange(len(self.teamScores)):
                        self.teamScores[i] = 0

                points = asss.call_callback(asss.CB_CTFWIN, (self.arena, freq, 0), self.arena)

                if points == 0: points = 12345
                flagcore.FlagReset(self.arena, freq, points)
                self.resetAllFlags()

        def getTeamName(self, freq):
                if 0 <= freq < self.config.teams:
                        return self.config.team[freq].name
                return 'Freq %d' % freq


def arenaToPlayerList(arena):
        def each(player):
                if player.arena == arena:
                        playerList.append(player)

        playerList = asss.PlayerListType()
        asss.for_each_player(each)
        return playerList


# called by pymod because it has a magic name
def mm_attach(arena):
        arena.fg_ctf = CTF(arena)
        arena.fg_ctf.attach()


# called by pymod because it has a magic name
def mm_detach(arena):
        if arena.fg_ctf:
                arena.fg_ctf.detach()

        delattr(arena, 'fg_ctf')
