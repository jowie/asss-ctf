import asss
from collections import namedtuple

cfg = asss.get_interface(asss.I_CONFIG)
game = asss.get_interface(asss.I_GAME)

ships = 'warbird', 'javelin', 'spider', 'leviathan', 'terrier', 'weasel', 'lancaster', 'shark'
PrizeSetting = namedtuple('PrizeSetting', 'setting prize')
prizes = (
        PrizeSetting('InitialBurst', asss.PRIZE_BURST),
        PrizeSetting('InitialRepel', asss.PRIZE_REPEL),
        PrizeSetting('InitialDecoy', asss.PRIZE_DECOY),
        PrizeSetting('InitialThor', asss.PRIZE_THOR),
        PrizeSetting('InitialBrick', asss.PRIZE_BRICK),
        PrizeSetting('InitialRocket', asss.PRIZE_ROCKET),
        PrizeSetting('InitialPortal', asss.PRIZE_PORTAL)
)


class CTFItemReset:
        def __init__(self, arena):
                self.arena = arena
                self.ctfScoreCallback = None
                self.ctfSaveCallback = None

        def attach(self):
                self.ctfScoreCallback = asss.reg_callback(asss.CB_CTFSCORE, self.ctfScore, self.arena)
                self.ctfSaveCallback = asss.reg_callback(asss.CB_CTFSAVE, self.ctfSave, self.arena)

        def detach(self):
                self.ctfScoreCallback = None
                self.ctfSaveCallback = None

        def ctfScore(self, arena, p, freq, enemyFreq, score):
                self.refreshItems(p)

        def ctfSave(self, arena, p, freq):
                self.refreshItems(p)

        def refreshItems(self, p):
                c = p.arena.cfg
                try:
                        ship = ships[p.ship]
                except IndexError:
                        return

                game.GivePrize(p, asss.PRIZE_FULLCHARGE, 1)

                for prize in prizes:
                        count = cfg.GetInt(c, ship, prize.setting, 0)
                        if count > 0:
                                game.GivePrize(p, -prize.prize, count)
                                game.GivePrize(p, prize.prize, count)


def mm_attach(arena):
        arena.ctf_itemreset = CTFItemReset(arena)
        arena.ctf_itemreset.attach()

def mm_detach(arena):
        if arena.ctf_itemreset:
                arena.ctf_itemreset.detach()

        delattr(arena, 'ctf_itemreset')
