#ifndef __FG_CTF_H
#define __FG_CTF_H

/* pyinclude: ctf/ctf.h */

/* called whenever a team scores a point
 * @threading called from main
 */
#define CB_CTFSCORE "ctfscore"
typedef void (*CTFScoreFunc)(Arena *arena, Player *p, int freq, int enemyFreq, int score);
/* pycb: arena, player, int, int, int */

/* called whenever a player picks up the flag of its own team
 * @threading called from main
 */
#define CB_CTFSAVE "ctfsave"
typedef void (*CTFSaveFunc)(Arena *arena, Player *p, int freq);
/* pycb: arena, player, int */

/* called to calculate the points awarded for a ctf-type flag game
 * win. handlers should increment *points.
 * @threading called from main
 */
#define CB_CTFWIN "ctfwin"
typedef void (*CTFWinFunc)(Arena *arena, int freq, int *points);
/* pycb: arena, int, int inout */

#endif

